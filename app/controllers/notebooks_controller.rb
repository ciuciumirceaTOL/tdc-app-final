class NotebooksController < ApplicationController
    include Response
    include ExceptionHandler
    before_action :set_notebook, only: [:show, :update, :destroy]
    
    def index
        @notebooks = Notebook.all
        json_response(@notebooks)
    end
    
    def show
        json_response(@notebook)
    end
    
    def create
        @notebook = Notebook.create!(notebook_params)
        json_response(@notebook, :created)
    end
    
    def update
        @notebook.update!(notebook_params)
        head :no_content
    end
    
    def destroy
        @notebook.destroy
        head :no_content
    end
    
    private
    
    def notebook_params
        params.permit(:name, :created_by)
    end
    
    def set_notebook
        @notebook = Notebook.find(params[:id])
    end
    
end
