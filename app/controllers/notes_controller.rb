class NotesController < ApplicationController
    include Response
    include ExceptionHandler
    before_action :set_notebook
    before_action :set_note, only: [:show, :update, :destroy]
    
    def index
        @notes = @notebook.notes
        json_response(@notes)
    end
    
    def show
        json_response(@note)
    end
    
    def create
        @note = @notebook.notes.create!(note_params)
        json_response(@note, :created)
    end
    
    def update
        @note.update!(note_params)
        head :no_content
    end
    
    def destroy
        @note.destroy
        head :no_content
    end
    
    private
    
    def set_notebook
        @notebook = Notebook.find(params[:notebook_id])
    end
    
    def set_note
        @note = @notebook.notes.find(params[:id])
    end
    
    def note_params
        params.permit(:title, :content)
    end
    
end
