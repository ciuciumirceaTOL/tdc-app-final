class Notebook < ApplicationRecord
    has_many :notes, dependent: :destroy
    validates_presence_of :name, :created_by
end
