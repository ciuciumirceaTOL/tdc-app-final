class CreateNotebooks < ActiveRecord::Migration[5.1]
  def change
    create_table :notebooks do |t|
      t.string :name
      t.string :created_by

      t.timestamps
    end
  end
end
