FactoryBot.define do
    factory :note do 
        title { Faker::Lorem.word }
        content { Faker::Lorem.paragraph }
        notebook nil
    end
end