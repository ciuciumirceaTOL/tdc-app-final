require 'rails_helper'

RSpec.describe 'Notes API', type: :request do
    
    let!(:notebook) { create(:notebook) }
    let!(:notes) { create_list(:note, 10, notebook_id: notebook.id) }
    let(:notebook_id) { notebook.id }
    let(:note_id) { notes.first.id }
    
    describe 'GET /notebooks/:id/notes' do
        
        before { get "/notebooks/#{notebook_id}/notes" } 
        
        context 'notebook exists' do

            it 'returns the list of notes' do 
                expect(json).not_to be_empty
                expect(json.size).to eq(10)
            end
        
            it 'returns status 200' do
                expect(response).to have_http_status(200)
            end
            
        end
        
        context 'notebook does not exist' do
            
            let(:notebook_id) { 1500 }
            
            it 'returns status 404' do
                expect(response).to have_http_status(404)
            end
            
            it 'return not found message' do
                expect(response.body).to match(/Couldn't find Notebook/)
            end
            
        end
        
    end
    
    describe 'GET /notebooks/:id' do
        
        before { get "/notebooks/#{notebook_id}/notes/#{note_id}" } 
        
        context 'note exists' do
        
            it 'returns the note' do 
                expect(json).not_to be_empty
                expect(json['id']).to eq(note_id)
            end
        
            it 'returns status 200' do
                expect(response).to have_http_status(200)
            end
            
        end
        
        context 'note does not exist' do
            
            let(:note_id) { 0 }
            
            it 'returns status 404' do
                expect(response).to have_http_status(404)
            end
            
            it 'returns not found message' do 
                expect(response.body).to match(/Couldn't find Note/)
            end
            
        end
        
    end
    
    describe 'POST /notebooks' do
        
        let(:valid_attributes) { { title: 'Learning TDD',  content: 'TDD is awesome'} }
        
        context 'request is valid' do
            
            before { post "/notebooks/#{notebook_id}/notes", params:  valid_attributes}
        
            it 'returns the note' do 
                expect(json).not_to be_empty
                expect(json['title']).to eq(valid_attributes[:title])
            end
        
            it 'returns status 201' do
                expect(response).to have_http_status(201)
            end
            
        end
        
        context 'request is not valid' do
            
            before { post "/notebooks/#{notebook_id}/notes", params:  {content: 'TDD is awesome'}}
        
            it 'returns status 422' do
                expect(response).to have_http_status(422)
            end
            
            it 'returns invalid attributes message' do 
                expect(response.body).to match(/Validation failed: Title can't be blank/)
            end
            
        end
        
    end
    
    describe 'PUT /notebooks/:id/notes/:id' do
        
        let(:valid_attributes) { { title: 'TDD' } }
        
        context 'request is valid' do
            
            before { put "/notebooks/#{notebook_id}/notes/#{note_id}", params: valid_attributes }
            
            it 'updates the note' do 
                note = Note.find(note_id)
                expect(note.title).to match(/TDD/)
            end
            
            it 'returns status 204' do
                expect(response).to have_http_status(204)
            end
            
        end
        
        context 'request is not valid' do
            
            before { put "/notebooks/#{notebook_id}/notes/#{note_id}", params: { title: '' } }
        
            it 'returns status 422' do
                expect(response).to have_http_status(422)
            end
            
            it 'returns invalid attributes message' do 
                expect(response.body).to match(/Validation failed: Title can't be blank/)
            end
            
        end
        
    end
    
    describe 'DELETE /notebooks/:id/notes/:id' do
        
        before { delete "/notebooks/#{notebook_id}/notes/#{note_id}" }
        
        it 'deletes the note' do
            note = Note.find_by_id(note_id)
            expect(note).to be_nil
        end
            
        it 'returns status 204' do
            expect(response).to have_http_status(204)
        end
            
    end
    
end
