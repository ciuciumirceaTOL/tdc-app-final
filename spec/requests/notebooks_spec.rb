require 'rails_helper'

RSpec.describe 'Notebooks API', type: :request do
    
    let!(:notebooks) { create_list(:notebook, 10) }
    let(:notebook_id) { notebooks.first.id }
    
    describe 'GET /notebooks' do
        
        before { get '/notebooks' } 

        it 'returns the list of notebooks' do 
            expect(json).not_to be_empty
            expect(json.size).to eq(10)
        end
        
        it 'returns status 200' do
            expect(response).to have_http_status(200)
        end
        
    end
    
    describe 'GET /notebooks/:id' do
        
        before { get "/notebooks/#{notebook_id}" } 
        
        context 'notebook exists' do
        
            it 'returns the notebook' do 
                expect(json).not_to be_empty
                expect(json['id']).to eq(notebook_id)
            end
        
            it 'returns status 200' do
                expect(response).to have_http_status(200)
            end
            
        end
        
        context 'notebook does not exist' do
            
            let(:notebook_id) { 0 }
            
            it 'returns status 404' do
                expect(response).to have_http_status(404)
            end
            
            it 'returns not found message' do 
                expect(response.body).to match(/Couldn't find Notebook/)
            end
            
        end
        
    end
    
    describe 'POST /notebooks' do
        
        let(:valid_attributes) { { name: 'Targul de cariere',  created_by: 'Mircea'} }
        
        context 'request is valid' do
            
            before { post '/notebooks', params:  valid_attributes}
        
            it 'returns the notebook' do 
                expect(json).not_to be_empty
                expect(json['name']).to eq(valid_attributes[:name])
            end
        
            it 'returns status 201' do
                expect(response).to have_http_status(201)
            end
            
        end
        
        context 'request is not valid' do
            
            before { post '/notebooks', params:  {name: 'Targul de cariere'}}
        
            it 'returns status 422' do
                expect(response).to have_http_status(422)
            end
            
            it 'returns invalid attributes message' do 
                expect(response.body).to match(/Validation failed: Created by can't be blank/)
            end
            
        end
        
    end
    
    describe 'PUT /notebooks/:id' do
        
        let(:valid_attributes) { { name: 'Targul de cariere' } }
        
        context 'request is valid' do
            
            before { put "/notebooks/#{notebook_id}", params: valid_attributes }
            
            it 'updates the notebook' do 
                notebook = Notebook.find(notebook_id)
                expect(notebook.name).to match(/Targul de cariere/)
            end
            
            it 'returns status 204' do
                expect(response).to have_http_status(204)
            end
            
        end
        
        context 'request is not valid' do
            
            before { put "/notebooks/#{notebook_id}", params: { name: '' } }
        
            it 'returns status 422' do
                expect(response).to have_http_status(422)
            end
            
            it 'returns invalid attributes message' do 
                expect(response.body).to match(/Validation failed: Name can't be blank/)
            end
            
        end
        
    end
    
    describe 'DELETE /notebooks/:id' do
        
        before { delete "/notebooks/#{notebook_id}" }
         
        it 'deletes the notebook' do
            notebook = Notebook.find_by_id(notebook_id)
            expect(notebook).to be_nil
        end
            
        it 'returns status 204' do
            expect(response).to have_http_status(204)
        end
        
            
    end
    
end
