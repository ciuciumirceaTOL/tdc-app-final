require 'rails_helper'

RSpec.describe Note, type: :model do
    it { should belong_to(:notebook) }
    it { should validate_presence_of(:title) }
end
