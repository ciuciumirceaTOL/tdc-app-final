require 'rails_helper'

RSpec.describe Notebook, type: :model do
  it { should have_many(:notes).dependent(:destroy) } 
  it { should validate_presence_of(:name) }
  it { should validate_presence_of(:created_by)}
end
